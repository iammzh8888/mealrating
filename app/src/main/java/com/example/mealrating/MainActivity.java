package com.example.mealrating;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements
        View.OnClickListener, AdapterView.OnItemSelectedListener{

    final static int REQUEST_CODE1 = 1;
    TextView textView;
    Spinner spinnerMeal;
    ImageView imageViewMeal;
    RatingBar ratingBarMeal;
    Button btnAdd, btnShowAll, btnMeal, btnSalad;
    boolean mealIsShown = true;

    String[] listMeal = {"Salmon", "Poutine", "Sushi", "Tacos"};
    int[] mealPicture = {R.drawable.salmon, R.drawable.poutine, R.drawable.sushi, R.drawable.tacos};
    String[] listSalad = {"Chicken salad", "Montreal", "Green salad"};
    int[] saladPicture = {R.drawable.chicken, R.drawable.fruit, R.drawable.green};

    ArrayList<MealRating> listOfMealRating;
    ArrayAdapter<String> mealAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {
        listOfMealRating = new ArrayList<>();

        textView = findViewById(R.id.textView);

        // Reference to ratingBar.................................
        ratingBarMeal = findViewById(R.id.ratingBar);
        //........................................................

        imageViewMeal = findViewById(R.id.imageView);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnShowAll = findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(this);

        btnMeal = findViewById(R.id.btnMeal);
        btnMeal.setOnClickListener(this);

        btnSalad = findViewById(R.id.btnSalad);
        btnSalad.setOnClickListener(this);

        // Initialize spinner -----------------------------------
        spinnerMeal = findViewById(R.id.spinnerMeal);
        spinnerMeal.setOnItemSelectedListener(this);

        mealAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                listMeal);
        spinnerMeal.setAdapter(mealAdapter);
        //-------------------------------------------------------
    }

    // AdapterView.OnItemSelectedListener ----------------------------------------------------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
        // 'parent' The AdapterView where the click happened
        // 'i' is index of selected item in spinner,
        // so we can assign the corresponding image reference
        // from our image array to our imageView
        int image;
        if(mealIsShown)
            image = mealPicture[i];
        else
            image = saladPicture[i];
        imageViewMeal.setImageResource(image);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }
    //----------------------------------------------------------------------------------------------

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnAdd:
                addMealRating();
                break;

            case R.id.btnShowAll:
                showAllMealRating();
                break;
            case R.id.btnMeal:
                showMeal();
                break;

            case R.id.btnSalad:
                showSalad();
                break;
        }
    }

    private void addMealRating() {
        String meal = spinnerMeal.getSelectedItem().toString();

        // Read ratingBar ....................................
        float rating  = (float) ratingBarMeal.getRating();
        //....................................................


        // Create new object and add it to our model array....
        MealRating mealRating = new MealRating(meal, rating);
        listOfMealRating.add(mealRating);
        //....................................................

        // Reset rating bar for next time
        ratingBarMeal.setRating(0);
    }

    private void showAllMealRating() {
        Bundle bundle =  new Bundle();
        bundle.putSerializable("ratingResults", listOfMealRating);

        Intent intent = new Intent(this, RatingResult.class);
        intent.putExtra("intentExtra", bundle);
        startActivityForResult(intent, REQUEST_CODE1);
    }

    private void showMeal() {
        spinnerMeal.setOnItemSelectedListener(this);

        mealAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                listMeal);
        spinnerMeal.setAdapter(mealAdapter);
        mealIsShown = true;
    }

    private void showSalad() {
        spinnerMeal.setOnItemSelectedListener(this);

        mealAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item,
                listSalad);
        spinnerMeal.setAdapter(mealAdapter);
        mealIsShown = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE1) {
            String receivedData = (String) data.getStringExtra("return_result_tag");

            if (resultCode == RESULT_OK)
                // Check GPS
                // Save data to DB
                // Call and end point
                textView.setText(receivedData);
            else
                textView.setText("Concordia meal rating");
        }
    }
}