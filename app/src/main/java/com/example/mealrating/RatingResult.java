package com.example.mealrating;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;


public class RatingResult extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    RadioButton radioButton1Star, radioButton2Stars,radioButton3Stars,radioButtonAscending,radioButtonDescending;
    TextView textViewResults;
    EditText editTextRegister;
    Button btnGoBack;

    ArrayList<MealRating> listOfMealRatings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_result);
        initialize();
    }

    private void initialize() {

        listOfMealRatings = new ArrayList<MealRating>();
        // Reference to Radio button ...................................
        radioButton1Star = findViewById(R.id.oneStar);
        radioButton1Star.setOnCheckedChangeListener(this);
        radioButton2Stars = findViewById(R.id.twoStars);
        radioButton2Stars.setOnCheckedChangeListener(this);
        radioButton3Stars = findViewById(R.id.threeStars);
        radioButton3Stars.setOnCheckedChangeListener(this);
        radioButtonAscending = findViewById(R.id.ascending);
        radioButtonAscending.setOnCheckedChangeListener(this);
        radioButtonDescending = findViewById(R.id.descending);
        radioButtonDescending.setOnCheckedChangeListener(this);

        // Reference to Rating Results.................................
        textViewResults = findViewById(R.id.textViewResults);

        // Reference to Register edit text.................................
        editTextRegister = findViewById(R.id.editTextRegister);

        btnGoBack = findViewById(R.id.btnGoBack);
        listRatingResults();
    }

    private void listRatingResults(){
        Bundle bundle = getIntent().getBundleExtra("intentExtra");
        listOfMealRatings = (ArrayList<MealRating>) bundle.getSerializable("ratingResults");

        String str = "";

        for(MealRating oneMeal : listOfMealRatings){
            str = str + oneMeal + "\n";
        }
        textViewResults.setText(str);
    }

    public void goBack(View view){
        if(editTextRegister.getText() != null && !editTextRegister.getText().toString().equals("")){
            String strRegister = editTextRegister.getText().toString();
            //------------------------------------ Create an intent and putExtra result string
            Intent intent = new Intent();
            intent.putExtra("return_result_tag", strRegister);

            //------------------------------------ Set Result for MainActivity
            setResult(RESULT_OK, intent);
            finish();
        }else {
            Toast toastWarning = Toast.makeText(this, "Must enter a name", Toast.LENGTH_SHORT);
            toastWarning.show();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView.isChecked()){
            switch (buttonView.getId()) {
                case R.id.oneStar:
                    gotoOneStar();
                    break;

                case R.id.twoStars:
                    gotoTwoStars();
                    break;

                case R.id.threeStars:
                    gotoThreeStars();
                    break;

                case R.id.ascending:
                    gotoAscending();
                    break;

                case R.id.descending:
                    gotoDescending();
                    break;
            }
        }
    }

    private void gotoOneStar(){
        String str = "";

        for(MealRating oneMeal : listOfMealRatings){
            if(oneMeal.getRating() >=1 && oneMeal.getRating() <2 ){
                str = str + oneMeal + "\n";
            }
        }
        textViewResults.setText(str);
    }

    private void gotoTwoStars(){
        String str = "";

        for(MealRating oneMeal : listOfMealRatings){
            if(oneMeal.getRating() >=2 && oneMeal.getRating() <3 ){
                str = str + oneMeal + "\n";
            }
        }
        textViewResults.setText(str);
    }

    private void gotoThreeStars(){
        String str = "";

        for(MealRating oneMeal : listOfMealRatings){
            if(oneMeal.getRating() ==3 ){
                str = str + oneMeal + "\n";
            }
        }
        textViewResults.setText(str);
    }

    private void gotoAscending(){
        Collections.sort(listOfMealRatings);

        String str = "";

        for(MealRating oneMeal : listOfMealRatings){
            str = str + oneMeal + "\n";
        }
        textViewResults.setText(str);
    }

    private void gotoDescending(){
        Collections.reverse(listOfMealRatings);

        String str = "";

        for(MealRating oneMeal : listOfMealRatings){
            str = str + oneMeal + "\n";
        }
        textViewResults.setText(str);
    }
}