package com.example.mealrating;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class MealRating implements Comparable, Serializable {

    private String mealName;
    private float rating;

    public MealRating(String mealName, float rating) {
        this.mealName = mealName;
        this.rating = rating;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Customer Order: " + mealName + ", Rating: " + rating;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        MealRating otherObject = (MealRating) o;
        return mealName.compareTo(otherObject.getMealName());
    }
}